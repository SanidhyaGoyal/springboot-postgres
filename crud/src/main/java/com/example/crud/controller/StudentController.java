package com.example.crud.controller;

import com.example.crud.model.Student;
import com.example.crud.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("students")
    public List<Student> getStudents(){
        return  studentRepository.findAll();
    }

    @PostMapping("insert")
    public Student createStudent(@RequestBody Student student){
        return studentRepository.save(student);
    }

    @GetMapping("students/{id}")
    public Optional<Student> getStudentById(@PathVariable(value = "id") int id){
        return  studentRepository.findById(id);

    }

    @PutMapping("/students/{id}")
    public Student updateStudent(@PathVariable(value="id") int id, @RequestBody Student studentDetails){
        Student student = studentRepository.findById(id).orElseThrow();

        student.setId(studentDetails.getId());
        student.setAge(studentDetails.getAge());
        student.setName(studentDetails.getName());

        Student updateStudent = studentRepository.save(student);
        return updateStudent;
    }
    @DeleteMapping("/students/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable(value = "id") int id){
        Student student= studentRepository.findById(id).orElseThrow();
        studentRepository.delete(student);

        return ResponseEntity.ok().build();
    }

}